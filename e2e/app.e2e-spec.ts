import { FirstNgProPage } from './app.po';

describe('first-ng-pro App', () => {
  let page: FirstNgProPage;

  beforeEach(() => {
    page = new FirstNgProPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
